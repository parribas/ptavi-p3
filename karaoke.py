#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
import sys
from xml.sax import make_parser
from smallsmilhandler import SmallSMILHandler

if __name__ == "__main__":
    """
          Programa principal
       """
    parser = make_parser()
    sHandler = SmallSMILHandler()
    parser.setContentHandler(sHandler)
    tags = sHandler.get_tags()
    fichero_smil = sys.argv[1]

    parser.parse(open(fichero_smil))
    try:

        fichero_json = fichero_smil.replace('.smil', '.json')
        with open(fichero_json, 'w') as out_file:
            json.dump(tags, out_file)

        for atributos in tags:
            for tags in atributos:
                if atributos[tags] != '':
                    print(tags, '=', atributos[tags], end='/t')
            print(end='...')
            print('/n')

    except:
        print("Usage: python3 karaoke.py file.smil.")
